#!/bin/sh
cmd //C CALL "C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\Common7\Tools\vsdevcmd.bat" -arch=x64 -host_arch=x64 -no_logo '&&' \
    ml64 //nologo //Fo $4.obj //c //Ta $4 '&&' \
    cl //Zi //Fe: $3 $4.obj $5
